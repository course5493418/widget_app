import 'package:flutter/material.dart';

class MenuItem {
  final String title;
  final String subtitle;
  final String link;
  final IconData icon;

  const MenuItem(
      {required this.title,
      required this.subtitle,
      required this.link,
      required this.icon});
}

const appMenuItems = <MenuItem>[
  MenuItem(
      title: 'Counter',
      subtitle: 'App counter numbers',
      link: '/counter',
      icon: Icons.countertops),
  MenuItem(
      title: 'Botones',
      subtitle: 'Varios botones',
      link: '/buttons',
      icon: Icons.smart_button_outlined),
  MenuItem(
      title: 'Tarjetas',
      subtitle: 'Un contenedor',
      link: '/cards',
      icon: Icons.credit_card),
  MenuItem(
      title: 'Progress Indicators',
      subtitle: 'Generales y controlados',
      link: '/progress',
      icon: Icons.refresh_rounded),
  MenuItem(
      title: 'SnackBars and dialogs',
      subtitle: 'Indicadores en pantalla',
      link: '/snackbars',
      icon: Icons.info_outline),
  MenuItem(
      title: 'Animated contanier',
      subtitle: 'Statefull widget animated',
      link: '/animated',
      icon: Icons.check_box_outline_blank_outlined),
  MenuItem(
      title: 'UI Controls',
      subtitle: 'Controls UI',
      link: '/ui-controls',
      icon: Icons.radio_button_checked),
  MenuItem(
      title: 'Introduction',
      subtitle: 'App inicial tutotial',
      link: '/tutorial',
      icon: Icons.accessibility_sharp),
  MenuItem(
      title: 'Infinite Scroll and Pull',
      subtitle: 'App Infinite Scroll',
      link: '/infinite',
      icon: Icons.list_alt_rounded),
  MenuItem(
      title: 'Change theme App',
      subtitle: 'App theme',
      link: '/theme-changer',
      icon: Icons.color_lens_outlined)
];
