import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class SnackbarScreen extends StatelessWidget {
  static const name = 'snackbar_screen';
  const SnackbarScreen({super.key});

  void showCustomSnackbar(BuildContext context) {
    ScaffoldMessenger.of(context).clearSnackBars();
    final snackbar = SnackBar(
      content: const Text('Hello World'),
      action: SnackBarAction(label: 'Continue', onPressed: () {}),
      duration: const Duration(seconds: 3),
    );
    ScaffoldMessenger.of(context).showSnackBar(snackbar);
  }

  void openDialog(BuildContext context) {
    showDialog(
      barrierDismissible: false,
        context: context,
        builder: (context) => AlertDialog(
              title: const Text('Saludos'),
              content: const Text(
                  'Quam viverra cubilia facilisis fusce interdum placerat ornare ultrices hendrerit velit, vivamus quis cursus auctor etiam nisl facilisi iaculis penatibus tempor, libero tristique tortor rutrum morbi et molestie dui accumsan. Ut vivamus nunc dictumst lobortis dapibus libero etiam augue, vulputate hendrerit cras taciti sodales justo rutrum torquent curabitur, aenean ac habitasse maecenas facilisis ridiculus nisi. Sem a conubia ornare tortor at commodo aptent, tempus class lectus bibendum tempor sed nunc, blandit curabitur libero molestie mauris nascetur.'),
              actions: [
                TextButton(
                    onPressed: () {
                      context.pop();
                    },
                    child: const Text('Cancel')),
                FilledButton(
                    onPressed: () {
                      context.pop();
                    },
                    child: const Text('Continue'))
              ],
            ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Snackbars and Dialogs'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            FilledButton.tonal(
                onPressed: () {
                  showAboutDialog(context: context, children: [
                    const Text(
                        'Lorem ipsum dolor sit amet consectetur adipiscing elit vivamus, pharetra ornare leo dictum fermentum integer. Mauris rhoncus augue varius platea himenaeos eros gravida, potenti proin class metus nibh convallis posuere aptent, nam ad orci elementum quam fermentum. Risus semper leo massa aliquet imperdiet integer maecenas viverra, elementum donec quam aliquam fermentum nostra pharetra habitant, facilisi volutpat interdum ultrices praesent nunc magnis.')
                  ]);
                },
                child: const Text('Licencias')),
            FilledButton.tonal(
                child: const Text('Dialog'),
                onPressed: () => openDialog(context))
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        label: const Text('Snackbar'),
        icon: const Icon(Icons.remove_red_eye_outlined),
        onPressed: () => showCustomSnackbar(context),
      ),
    );
  }
}
